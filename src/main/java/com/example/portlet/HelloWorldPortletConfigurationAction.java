package com.example.portlet;

import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;

import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.util.ParamUtil;

import aQute.bnd.annotation.metatype.Configurable;

@Component(
		configurationPid = "com.example.portlet.HelloWorldPortletConfiguration",
		configurationPolicy = ConfigurationPolicy.OPTIONAL,
		immediate = true,
		property = {
				"javax.portlet.name=com_example_portlet_HelloWorldPortlet"
		},
		service = ConfigurationAction.class)
public class HelloWorldPortletConfigurationAction extends DefaultConfigurationAction {
	private volatile HelloWorldPortletConfiguration config;
	
	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse)
			throws Exception {
		String preferredName = ParamUtil.getString(actionRequest, "preferredName");
		
		setPreference(actionRequest, "preferredName", preferredName);
		
		super.processAction(portletConfig, actionRequest, actionResponse);
	}
	
	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		request.setAttribute(HelloWorldPortletConfiguration.class.getName(), config);
		
		super.include(portletConfig, request, response);
	}
	
	@Activate
	@Modified
	protected void activate(Map<Object, Object> props) {
		config = Configurable.createConfigurable(HelloWorldPortletConfiguration.class, props);
	}
}

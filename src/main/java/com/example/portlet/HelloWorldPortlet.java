package com.example.portlet;

import java.io.IOException;
import java.util.Map;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.PortalUtil;

import aQute.bnd.annotation.metatype.Configurable;

@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	configurationPid = "com.example.portlet.HelloWorldPortletConfiguration",
	service = Portlet.class
)
public class HelloWorldPortlet extends MVCPortlet {
	private volatile HelloWorldPortletConfiguration config;
	
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		try {
			User user = PortalUtil.getUser(renderRequest);
			String userName = "Unknown person";
			
			if (user != null) {
				userName = user.getFirstName();
			}
			
			renderRequest.setAttribute("userName", userName);
		} catch (PortalException e) {
			e.printStackTrace();
		}
		
		super.render(renderRequest, renderResponse);
	}
	
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		renderRequest.setAttribute(HelloWorldPortletConfiguration.class.getName(), config);
		
		super.doView(renderRequest, renderResponse);
	}
	
	@Activate
	@Modified
	protected void activate(Map<Object, Object> props) {
		config = Configurable.createConfigurable(HelloWorldPortletConfiguration.class, props);
	}
}

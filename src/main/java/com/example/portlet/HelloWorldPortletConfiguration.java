package com.example.portlet;

import aQute.bnd.annotation.metatype.Meta;

@Meta.OCD(
		id = "com.example.portlet.HelloWorldPortletConfiguration",
		name = "hello-world.portlet.configuration.caption")
public interface HelloWorldPortletConfiguration {
	@Meta.AD(
			required = false,
			name = "hello-world.portlet.configuration.preferred-name")
	public String preferredName();
}

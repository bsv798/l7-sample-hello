<%@ include file="init.jsp" %>

<jsp:useBean id="userName" class="java.lang.String" scope="request" />

<p>
	<b><liferay-ui:message key="hello-world.caption" /></b>
	<p><liferay-ui:message key="hello-world.greeting" arguments="<%= userName %>" /></p>
	<p><liferay-ui:message key="hello-world.system-preferred-name" arguments="<%= systemPreferredName %>" /></p>
	<p><liferay-ui:message key="hello-world.portlet-preferred-name" arguments="<%= portletPreferredName %>" /></p>
</p>

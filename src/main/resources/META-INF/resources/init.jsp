<%@page import="com.example.portlet.HelloWorldPortletConfiguration"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<liferay-theme:defineObjects />

<portlet:defineObjects />

<%
	HelloWorldPortletConfiguration mySystemConfig = (HelloWorldPortletConfiguration) renderRequest.
			getAttribute(HelloWorldPortletConfiguration.class.getName());
	String systemPreferredName = mySystemConfig.preferredName();
	
	if ((systemPreferredName == null) || (systemPreferredName.isEmpty())) {
		systemPreferredName = "not set";
	}
    HelloWorldPortletConfiguration myPortletConfig = portletDisplay
    		.getPortletInstanceConfiguration(HelloWorldPortletConfiguration.class);
    String portletPreferredName = myPortletConfig.preferredName();
    
    if ((portletPreferredName == null) || (portletPreferredName.isEmpty())) {
    	portletPreferredName = "not set";
    }
%>
